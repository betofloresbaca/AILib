# -*- coding: utf-8 -*-
'''Artificial neural networks framework implementetion'''


#-------------------------------------------------------------------------------
#                               Imports
#-------------------------------------------------------------------------------

import sys
import copy
import numpy as np
from random import uniform
from PySide import QtGui, QtCore


#-------------------------------------------------------------------------------
#                               Exceptions
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
#                            Activation functions
#-------------------------------------------------------------------------------

def sigmoide_af(arr):
    return 1.0 / (1.0 + np.exp(arr * -1.0))

def sigmoide_daf(s): #Reccibe la sigmoide a la entrada
    return s*(1 - s)

#-------------------------------------------------------------------------------
#                               Main Classes
#-------------------------------------------------------------------------------

class ArtificialNeuralNetwork:

    def __init__(self, layers, af, daf, lf, b):
        #Tomando el factor de aprendizaje
        self.learn_factor = lf
        #Tomando el bias
        self.bias = b
        #Creando los pesos de los bias
        self.w_bias = np.array([
                                     [      1.0 #Para pruebas
                                        for l in range(layers[k])
                                     ]
                                  for k in range(1, len(layers))
                               ])
        #Creando la matriz de neuronas con valor 0
        self.n = np.array([[0]*k for k in layers])
        #Creando la matriz de pesos
        self.w = np.array([
                     [              
                         [       #Un numero aleatorio entre 0 y 1
                                 #uniform(0, 1)
                                 0
                                 #1 #Para pruebas
                             #Para cada coneccionn desde la capa anterior
                             for x in range(layers[kl-1])
                         ]
                         #Para cada neurona en cada capa 
                         for ln in range(layers[kl])
                     ]
                     #Para cada capa a partir de la segunda
                     for kl in range(1, len(layers))
                 ])
        #Estableciendo la funcion de activacion
        self.af = af
        #Estableciendo la derivada de la funcion de activacion
        self.daf = daf
        
    def __think(self):
        #Para todas las capas despues de la capa de entrada)
        for x in range(1,len(self.n)):
            #Para cada neurona en la capa se calcula la sumatoria de entradas
            #multiplicadas por los pesos y tambien la del bias
            for y in range(len(self.n[x])):
                #Sumando las contribuciones de la capa
                self.n[x][y] = np.sum(np.array(self.n[x-1]) * np.array(self.w[x-1][y]))
                #Sumando la contribucion del bias
                self.n[x][y] += self.bias*self.w_bias[x-1][y]
            #Aplicando la funcion de activacion a la capa completa
            self.n[x] = (self.af(np.array(self.n[x]))).tolist()
        print("Think n final:",self.n[-1])
        print("Think valor esperado::",self.n[0])
        
    #Regresa la salida
    def processSignal(self, inputSignal):
        if(len(inputSignal) != len (self.n[0])):
            return ######## ToDo Exception
        #Poniendo la entrada al inicio de la red neuronal
        self.n[0] = inputSignal
        #Evaluando hacia adelante
        self.__think()
        return self.n[-1]

    def backPropagation(self, target): #Target es una lista
        if(len(target) != len(self.n[-1])):
            return ######## ToDo Exception
        #Calculando el error  de la capa final
        error_total = (np.array(target) - np.array(self.n[-1]))
        #Haciendo la matriz para los errores
        delta = np.array([[0]*len(self.n[k])  for k in range(1,len(self.n))])
        #Haciendo las matrices para los cambios en los pesos
        delta_w = np.array(copy.deepcopy(self.w.tolist()))
        delta_w_bias = np.array(copy.deepcopy(self.w_bias.tolist()))
        #Calculando el error de la ultima capa
        delta[-1] = ((error_total)*self.daf(np.array(self.n[-1]))).tolist()
        #calculando delta w en la ultima capa
        for in_i in range(len(delta_w[-1])):
            for in_j in range(len(delta_w[-1][in_i])):
                delta_w[-1][in_i][in_j] = delta[-1][in_i]*self.n[-2][in_j]*self.learn_factor
        #calculando el delta w del bias
        delta_w_bias[-1] = (np.array(delta[-1])*self.bias*self.learn_factor).tolist() 
        #Calculando para capas ocultas
        for k in range(len(self.n)-2, 0, -1):# Desde la penultima a la 1
            #### Los deltas de la red
            #Calculando la derivada de la capa k para delta
            delta[k-1] = self.daf(np.array(self.n[k])).tolist()
            #Para cada neurona l en la capa k
            for l in range(len(self.n[k])):
                delta[k-1][l] *= np.sum(np.array(delta[k])*np.array((np.array(self.w[k])[:,l]).tolist()))
            #### Los deltas w de la red para ese nivel
            for in_i in range(len(delta_w[k-1])):
                for in_j in range(len(delta_w[k-1][in_i])):
                    delta_w[k-1][in_i][in_j] = delta[k-1][in_i]*self.n[k-1][in_j]*self.learn_factor
            # El delta w del bias
            delta_w_bias[k-1] = (np.array(delta[k-1])*self.bias*self.learn_factor).tolist() 
        ###Actualizando los valores de los pesos
        #De los pesos de la red
        for k in range(len(self.w)):
            self.w[k] = (np.array(self.w[k]) + np.array(delta_w[k])).tolist()
        #De los pesos de los bias
        for k in range(len(self.w_bias)):
            self.w_bias[k] = (np.array(self.w_bias[k]) + np.array(delta_w_bias[k])).tolist()
        ##Regresa el error de la iteracion
        error_total = np.sum(error_total*error_total)
        return error_total

    def learn(self, inputs, outputs, max_error, max_iter): #recibe las entradas y las salidas deseadas
        #Mientras el error es mayor que el maximo permitido
        error = 0
        for ind_i in range(max_iter):
            print("iter",ind_i)
            error = 0
            #Para cada entrada obtener el error hacia adelante y ajustar pesos 
            for ind_j in range(len(inputs)):
                self.processSignal(inputs[ind_j]) #Hacia adelante
                error += self.backPropagation(outputs[ind_j])
            if error<=max_error:
                break
        return error
    

#Para identificacion de caracteres
class CaracterIdentifier(QtGui.QWidget):

    def __init__(self):
        QtGui.QWidget.__init__(self)
        self.initUI()

    def initUI(self):
        #Las etiquetas
        cargarLabel = QtGui.QLabel('Carga de patrones')
        cargarLabel.setStyleSheet("QLabel {color : blue;}")
        archivoLabel = QtGui.QLabel('Archivo:')
        entrenamientoLabel = QtGui.QLabel('Entrenamiento')
        entrenamientoLabel.setStyleSheet("QLabel {color : blue;}")
        maxErrorLabel = QtGui.QLabel('Max error:')
        maxIterLabel = QtGui.QLabel('Max iteraciones:')
        #iterLabel = QtGui.QLabel('Iteración')
        entradaLabel = QtGui.QLabel('Entrada')
        entradaLabel.setStyleSheet("QLabel {color : blue;}")
        salidaLabel = QtGui.QLabel('Salida')
        salidaLabel.setStyleSheet("QLabel {color : blue;}")
        #Los campos de entrada
        self.archivoLE = QtGui.QLineEdit("paterns.ann_in")
        self.maxErrorLE = QtGui.QLineEdit("0.1")
        self.maxIterLE = QtGui.QLineEdit("100")
        self.outLE = QtGui.QLineEdit("")
        self.outLE.setReadOnly(True)
        #Etiquetas de entrada
        self.labelsInput = []
        for ind_i in range(56):
            self.labelsInput.append(QtGui.QPushButton(""))
            self.labelsInput[-1].setStyleSheet("QPushButton {background-color : white;}")
            self.labelsInput[-1].clicked.connect(self.marcar)
        #Los botones
        cargarButton = QtGui.QPushButton('Cargar')
        limpiarButton = QtGui.QPushButton('Limpiar')
        entrenarButton = QtGui.QPushButton('Entrenar')
        consultarButton = QtGui.QPushButton('Consultar')
        #The signals
        cargarButton.clicked.connect(self.loadFile)
        entrenarButton.clicked.connect(self.learn)
        limpiarButton.clicked.connect(self.limpiarPanel)
        consultarButton.clicked.connect(self.query)
        #El grid de la carga
        cargarGrid = QtGui.QGridLayout()
        cargarGrid.setSpacing(5)
        cargarGrid.addWidget(cargarLabel,0,0,1,2)
        cargarGrid.addWidget(archivoLabel,1,0)
        cargarGrid.addWidget(self.archivoLE,1,1)
        cargarGrid.addWidget(cargarButton,2,1)
        #El grid del entrenamiento
        entrenarGrid = QtGui.QGridLayout()
        entrenarGrid.setSpacing(5)
        entrenarGrid.addWidget(entrenamientoLabel,0,0,1,2)
        entrenarGrid.addWidget(maxErrorLabel,1,0)
        entrenarGrid.addWidget(self.maxErrorLE,1,1)
        entrenarGrid.addWidget(maxIterLabel,2,0)
        entrenarGrid.addWidget(self.maxIterLE,2,1)
        entrenarGrid.addWidget(entrenarButton,3,1)
        #El grid de la entrada
        entradaGrid = QtGui.QGridLayout()
        entradaGrid.setSpacing(5)
        entradaGrid.addWidget(entradaLabel,0,0,1,3)
              #Grid de input
        panel = QtGui.QGridLayout()
        panel.setHorizontalSpacing(0)
        panel.setVerticalSpacing(0)
        for ind_i in range(8):
            for ind_j in range(7):
                panel.addWidget(self.labelsInput[ind_i*7+ind_j],ind_i, ind_j)
        entradaGrid.addLayout(panel,1,0,8,7)
        entradaGrid.addWidget(limpiarButton,9,0,1,3)
        entradaGrid.addWidget(consultarButton,9,3,1,3)
        #El grid de la salida
        salidaGrid = QtGui.QGridLayout()
        salidaGrid.setSpacing(5)
        salidaGrid.addWidget(salidaLabel,0,0)
        salidaGrid.addWidget(self.outLE,0,1)        
        #The grid layout general
        grid = QtGui.QGridLayout()
        grid.setSpacing(15)
        grid.addLayout(cargarGrid,0,0,1,1)
        grid.addLayout(entrenarGrid,0,1,1,1)
        grid.addLayout(entradaGrid,1,0,3,1)
        grid.addLayout(salidaGrid,2,1,1,1)
        ############FIN LAYOUTS ##################
        self.setLayout(grid)
        self.setGeometry(100, 100, 430, 400)
        self.setFixedSize(self.size());
        self.setWindowTitle('Reconocer Caracteres')
        self.show()
        self.limpiarPanel()
    
    def loadFile(self):
        #Tomando el sender
        sender = self.sender()
        #Tomando el nombre del archivo
        fName = self.archivoLE.text()
        #Creando los atributos necesarios
        self.c = [] #Lista de los caracteres
        self.inputs = []
        self.outputs = []
        #Abriendo el archivo de datos de entrenamiento
        with open(fName, "rt") as dataFile:
            line = dataFile.readline()
            (nChars, nRows, nCols) = list(map(lambda x: int(x), line.split()))
            #print(nChars, nRows, nCols)
            for ind_i in range(nChars):
                #Construyendo la salida correspondiente
                self.outputs.append([0]*(nChars+1))
                self.outputs[ind_i][ind_i+1] = 1 ####
                #Lemos el simbolo objetivo
                line = dataFile.readline()[:-1]
                print("caracter",line)
                self.c.append(line)
                #Ponemos una lista para la matriz
                self.inputs.append([])
                #Leemos toda la matriz
                for ind_j in range(nRows):
                    #Leemos una linea
                    line = dataFile.readline()
                    digits = list(line)[:-1]
                    self.inputs[ind_i].extend(list(map(lambda x: float(x), digits)))
                    #construyendo la red neuronal artificial
            self.ann = ArtificialNeuralNetwork((nRows*nCols, 56, nChars+1), sigmoide_af, sigmoide_daf, 10, 1)
        
    def learn(self):
        #Tomando el sender
        sender = self.sender()
        #Tomando los parametros
        max_error = float(self.maxErrorLE.text())
        max_iter = int(self.maxIterLE.text())
        #Entrenando
        print("Entrenando")
        print("Inputs",self.inputs)
        print("Outputs",self.outputs)
        self.ann.learn(self.inputs, self.outputs, max_error, max_iter)
        #Fin
        print("Fin entrenamiento:")
        print("w",self.ann.w)
        print("n",self.ann.n)

    def query(self):
        #Tomando el sender
        sender = self.sender()
        #Tomando los parametros
        signal = self.listarEntrada()
        print("Entrada:", signal)
        #Procesando
        sig = self.ann.processSignal(signal)
        maximo = max(sig)
        indiceMax = sig.index(maximo)
        print("Vector Salida:",sig)
        print("Indice Max:",indiceMax)
        if indiceMax > 0:
            self.outLE.setText(str(self.c[indiceMax-1]))
        else:
            self.outLE.setText("No reconocido")

    def limpiarPanel(self):
        for label in self.labelsInput:
            label.setStyleSheet("QLabel {background-color : white;}")
            label.setText("")

    def listarEntrada(self):
        lista = []
        for label in self.labelsInput:
            if label.text()==".":
                lista.append(1.0)
            else:
                lista.append(0.0)
        return lista

    def marcar(self):
        sender = self.sender()
        if sender.text()==".":
            sender.setText("")
            sender.setStyleSheet("QPushButton {background-color : white;}")
        else:
            sender.setText(".")
            sender.setStyleSheet("QPushButton {background-color : black;}")

#-------------------------------------------------------------------------------
#                               Main
#-------------------------------------------------------------------------------

def main():
    app = QtGui.QApplication(sys.argv)
    w = CaracterIdentifier()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
